package config

const (
	DatabaseHostKey     = "DATABASE_HOST"
	DatabasePortKey     = "DATABASE_PORT"
	DatabaseUserKey     = "DATABASE_USER"
	DatabasePasswordKey = "DATABASE_PASSWORD"
	DatabaseDBNameKey   = "DATABASE_DBNAME"
)
